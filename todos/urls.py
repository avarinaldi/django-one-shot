from django.urls import path
from todos.views import TodoListListView, TodoListDetailView

urlpatterns = [
    path("<int:pk>/", TodoListDetailView.as_view(), name="show_todolist"),
    path("", TodoListListView.as_view(), name="list_todos"),
]
